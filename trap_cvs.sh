#/bin/bash
mode=$1

np=4

# make sure that converged conduction velocities are correct in 3D for planar wavefronts
# measured varp conduction velocities are:
#
#  healthy myocardium: vf = 0.857 vs,n = 0.476
#  borderzone        : vf = 0.593 vs,n = 0.107

# test planar wavefront velocities
tag="trap_cvs"
dxs=("50" "100" "200" "300" "350" "400" "450" "500" "550" "600" "700")

for dx in ${dxs[@]}; do
  cmd="./tissue_init.py --experiment varp.json --resolution $dx --ID trap_cvs_dx_${dx} --tag ${tag}"
  echo $cmd
  
  if [ "$mode" != "--dry" ]; then 
    eval ${cmd}
  fi

done
