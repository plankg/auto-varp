-85.5942            # Vm
-                   # Lambda
-                   # delLambda
-                   # Tension
-                   # K_e
-                   # Na_e
-                   # Ca_e
0.000759707         # Iion
-                   # tension_component
-                   # illum
TT2
3.12007             # CaSR
0.000356464         # CaSS
0.10291             # Ca_i
3.21236e-05         # D
0.672672            # F
0.937291            # F2
0.999354            # FCaSS
1.2338e-05          # GCaL
0.0459              # Gkr
0.0784              # Gks
0.294               # Gto
0.754609            # H
0.6698              # J
138.52              # K_i
0.00158752          # M
6.85121             # Na_i
2.27531e-08         # R
0.972237            # R_
0.999973            # S
0.0236645           # Xr1
0.474954            # Xr2
0.0201831           # Xs

