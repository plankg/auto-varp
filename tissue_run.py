#!/usr/bin/env python3

r"""
"""
import os, sys
from datetime import date
from json import load, dump
from shutil import copy
import numpy as np

from carputils import settings
from carputils import tools
from carputils import model
from carputils import mesh
import j2carp as j2c


EXAMPLE_DESCRIPTIVE_NAME = 'Prepacing and validation of tissue/organ models'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def parser():
    parser = tools.standard_parser()

    # experiment selection and generic overall options
    exp  = parser.add_argument_group('Experiment selection and general options')

    exp.add_argument('--config', type=str, required=True, default='conf.json',
                       help='Describe tissue model in terms of labels and function association.')

    exp.add_argument('--experiment', type=str, required=True, default='exp.json',
                       help='Macro parameter experiment description in .json format.')
    
    exp.add_argument('--stage', 
                       choices=['gen-lat', 'prepace', 'validate'], default='prepace',
                       help='Select stage: tune for velocities, prepace or validate prepaced state (default: tune).')

    exp.add_argument('--update', action='store_true',
                       help='Update conductivity and velocity settings in experiment description')

    # generation of lat args
    gl_args  = parser.add_argument_group('LAT-generation specific options')
    gl_args.add_argument('--gen-lat',
                       choices=['ek', 'rd'], default='off',
                       help='Generate an activation sequence to create a LAT vector.\n' +
                       'Choices are:\n' +
                       '(ek): use eikonal method to create an LAT vector \n' +
                       '(rd): use reaction-diffusion simulation to create an LAT vector.')

    # prepacing stage args
    pp_args  = parser.add_argument_group('Pre-pacing specific options')

    pp_args.add_argument('--prepace', 
                       choices=['off', 'lat', 'lat1', 'full'], default='off',
                       help='Select pre-pacing mode to find tissue/organ scale limit cycle.\n' +
                       'Choices are:\n' +
                       '(off): use single cell initial state only \n' +
                       '(lat): use lat-defined activation sequence \n' +
                       '(lat1): use lat-defined activation sequence + one full activation sequence to account for diffusion effects \n' +
                       '(full): brute force, run pre-pacing at tissue scale.')

    pp_args.add_argument('--lat', type=str, default=None,
                       help='Provide lat vector for prepacing.')

    return parser


# ------PREPACE -------------------------------------------------------

# return mesh extension as function of run
def mesh_ext(rmode):

    if rmode == 'ek':
        ext = '_ek'
    else:
        ext = '_i'

    return ext


def setup_lats_prepacing(pp_bcl, pp_cycs, lat_file):

    # compute lats sequence based on S1_elecs

    # assemble lats prepacing structure
    lats_pp = ['-prepacing_bcl',    pp_bcl,
               '-prepacing_beats',  pp_cycs,
               '-prepacing_lats',   lat_file]

    return lats_pp


# helper function to select stimulus mode
# voltage is more robust, but currently ignored in ek and re runs
# decide on stimulus type as function of propagation mode
def stim_mode(prop):

    # by defaul in rd runs, use voltage clamp to achieve robust capture
    s_type = 9
    s_strength = -30

    # eikonal mediated propagation picks up only transmembrane stimuli
    if prop == 'ek' or prop == 're':
        # eikonal-mediated triggers on transmembrane current stimuli
        s_strength = 250
        s_type     = 0

    return s_type, s_strength


# prepace tissue to generate initial state
def setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode):

    # unpack experimental settings
    pp_bcl    = pp_prtcl.get('bcl')
    pp_cyc    = pp_prtcl.get('cycles')
    pp_elecs  = pp_prtcl.get('electrodes')
    pp_rtimes = pp_prtcl.get('rel-timings')

    # check whether all prepacing electrodes are defined
    for e in pp_elecs:
        if e not in elecs:
            print('Electrode {} required for prepacing, but not defined.'.format(e))
            return None 

    # decide on stimulus type as function of propagation mode
    s_type, s_strength = stim_mode(rmode)

    # assign protocols to all electrodes used for pre-pacing
    stims = []
    for i, e in enumerate(pp_elecs):
        stims += ['-stim[{}].elec.vtx_file'.format(i),  elecs[e]['VTXLST'],
                  '-stim[{}].ptcl.start'.format(i),     pp_rtimes[i],
                  '-stim[{}].ptcl.duration'.format(i),  2,
                  '-stim[{}].pulse.strength'.format(i), s_strength, 
                  '-stim[{}].crct.type'.format(i),      s_type ]
    
    num_stims = len(pp_elecs)
    if rmode == 're':
        stims += ['-stim[{}].crct.type'.format(num_stims), 8 ]
        num_stims = num_stims + 1

    stims = ['-num_stim', num_stims] + stims

    # set up simulation command
    cmd  = tools.carp_cmd()
    cmd += stims

    # setup mesh
    splt = True # use split-list if available
    cmd += setup_mesh(meshname, splt) 

    # set up tissue properites
    cmd += j2c.setup_tissue(cnf, fncs, rmode)

    return cmd


# setup mesh-related command options
def setup_mesh(meshname, splt):

    cmd = []

    # use split information if available
    if splt:
        splt_file = meshname + '.splt'
        if os.path.exists(splt_file):
            cmd += [ '-use_splitting', 1 ]
        else:
            print('Splitting selected, but split file {} does not exist!'.format(splt_file))
            print('Proceed without splitting.')

    cmd += [ '-meshname', meshname ]

    return cmd

    
# generate lat vector bei running eikonal or rd
def generate_lat(meshname, cnf, fncs, elecs, pp_prtcl, gen_lat, lats_file, visual):

    cmd_lat = setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, gen_lat)

    splt = True 
    cmd_lat += setup_mesh(meshname, splt)

    # total activation time must be shorter than bcl
    # use bcl as an upper limit for duration of simulation
    # in the rd case it would be more efficient if simulator could detect total activation time
    dur = pp_prtcl.get('bcl')

    cmd_lat += [ '-tend', dur ]

    if gen_lat == 'ek':
        
        # generate lats by eikonal solve
        simID   = 'ek_lat'
        grdout  = 'ek'
        cmd_lat+= [ '-simID', simID, '-experiment', 6 ]

        # lat vector handle
        tmp_lats = os.path.join(simID, 'vm_act_seq.dat')

    if gen_lat == 'rd':

        # generate lats by rd solve
        simID    = 'rd_lat'
        grdout   = 'i'
        lats     = [ '-num_LATs', 1 ] + j2c.ek_lat_detect(0, -20)
        cmd_lat += ['-simID', simID, '-experiment', 0, '-spacedt', 1. ] + lats

        # lat vector handle
        tmp_lats = os.path.join(simID, 'init_acts_vm_act_seq.dat-thresh.dat')

    # prepare visualization
    if visual:
        vis = [ '-gridout_{}'.format(grdout), 3,
                '-spacedt', 1 ]
        cmd_lat += vis

    # generate activation sequcne
    return cmd_lat, tmp_lats, simID


# actual prepacing
def prepace(meshname, cnf, fncs, elecs, pp_prtcl, mode, lats_file, rmode, visual):

    # set up overall command line
    cmd = setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode)

    # unpack prepacing protocol
    pp_bcl = pp_prtcl.get('bcl')
    pp_cyc = pp_prtcl.get('cycles')
    
    # earliest stimulus timing
    stim_trg_timings = pp_prtcl.get('rel-timings')
    start = min(stim_trg_timings)
    stop  = max(stim_trg_timings)

    # specialize command for different prepacing strategies
    chkpt   = []
    lats_pp = []
    simID   = None
    t_chk   = 0
    statef  = None
    stims   = []
    if mode == 'off':
        simID = 'prepace_off'
        chkpt = ['-num_tsav', 1, '-tsav[0]', t_chk]

        # we write only an initial checkpoint, no actual prepacing
        # we turn off all stimuli in this case
        for i, e in enumerate(elecs):
            stims += ['-stim[{}].ptcl.start'.format(i),    0,
                      '-stim[{}].ptcl.npls'.format(i),     0,
                      '-stim[{}].ptcl.duration'.format(i), 0 ]

        cmd += [ '-tend', 1., '-spacedt', 0.1 ]

    elif mode == 'lat':

        # use fast lats-based pre-pacing
        simID   = 'prepace_lat'
        chkpt   = ['-num_tsav', 1, '-tsav[0]', t_chk] + [ '-num_stim', 0]
        lats_pp = setup_lats_prepacing(pp_bcl, pp_cyc, lats_file)

        # turn off any stimuli as in the off case
        for i, e in enumerate(elecs):
            stims += ['-stim[{}].ptcl.start'.format(i),    0,
                      '-stim[{}].ptcl.npls'.format(i),     0,
                      '-stim[{}].ptcl.duration'.format(i), 0 ]
    
        cmd += [ '-tend', 1., '-spacedt', 0.1 ]

    elif mode == 'lat1':

        # use fast lats-based pre-pacing + one full activation sequence to account for diffusion
        simID   = 'prepace_lat1'
        chkpt   = ['-num_tsav', 1, '-tsav[0]', t_chk] + [ '-num_stim', 0]
        lats_pp = setup_lats_prepacing(pp_bcl, pp_cyc, lats_file)

        # we run one beat to account for diffusion effects
        for i, e in enumerate(elecs):
            stims += ['-stim[{}].ptcl.bcl'.format(i),  pp_bcl,
                      '-stim[{}].ptcl.npls'.format(i), 1 ]
    
        # adjust simulation duration
        cmd += [ '-tend', pp_bcl + stop, '-spacedt', 1. ]

    else :  # mode == 'full':
        # no LAT vector needed, we do brute force pacing
        print('Full prepacing procedure')
        simID = 'prepace_full'

        # checkpoint at t=0 ms
        t_chk  = pp_bcl * pp_cyc + start

        # add prepacing protocol to stimuli
        for i, e in enumerate(elecs):
            stims += ['-stim[{}].ptcl.bcl'.format(i),  pp_bcl,
                      '-stim[{}].ptcl.npls'.format(i), pp_cyc ]

        # adjust simulation duration
        cmd += [ '-tend', pp_bcl*pp_cyc + start, '-spacedt', 0.1 ]


    vis = []
    if visual:
        vis += [ '-gridout_i', 3,
                 '-spacedt',   1 ]

    statef = 'state.%.1f'%(t_chk)
    chkpt  = ['-num_tsav', 1, '-tsav[0]', t_chk, '-write_statef', 'state']
    cmd   += stims
    cmd   += chkpt
    cmd   += lats_pp
    # input forces simulation beyond the end of latest stimulus
    # this is efficient, but input logic doesn't allow for early stopping with a tend of 0
    cmd   += ['-simID', simID, '-experiment', 0 ] + vis 

    return cmd, statef, simID


# pick run mode 'rd' or 're'
def restart(meshname, cnf, fncs, elecs, pp_prtcl, mode, rmode, restart_chkpt, visual):

    # set up overall command line
    cmd = setup_prepace(meshname, cnf, fncs, elecs, pp_prtcl, rmode)

    simID = 'validate_pp_{}'.format(mode)

    # determine checkpoint timing
    pp_bcl = pp_prtcl['bcl']
    if mode == 'full':
        pp_cyc = pp_prtcl['cycles']
        t_chk  = pp_bcl * pp_cyc
    else:
        t_chk  = 0

    # determine checkpoint to restart from
    cmd += ['-start_statef', restart_chkpt]
    
    cmd += ['-simID', simID, '-experiment', 0]

    # simulate one bcl
    cmd += [ '-tend', t_chk + pp_bcl ]

    vis = []
    if visual:
        vis = [ '-gridout_i', 3,
                '-spacedt',   1 ]

    cmd += vis

    return cmd, simID


# print a summary of prepacing protocol
def print_prepace_summary(cnf, exp, prepace, lat, rstrt_file):

    fncs    = exp.get('domains')
    elecs   = exp.get('electrodes')
    prtcls  = exp.get('protocols')
    pp      = prtcls.get('prepace')

    # determine used lat vector
    pp_lat = lat 
    if lat is None:
        pp_lat = pp.get('lat')

    if prepace == 'off' or prepace == 'full':
        pp_lat = None

    hd = 'Prepacing Summary:'
    print('\n{}'.format(hd))
    print('='*len(hd) + '\n')

    # method used to compute prepaced state
    print('Prepacing mode: {}'.format(prepace))

    # protocol description
    print('{:14s}: {}'.format('Basic cycle length', pp.get('bcl')))
    print('{:14s}: {}'.format('#cycles', pp.get('cycles')))


    # state after prepacing stored in checkpoint
    print('{:14s}: {}'.format('Checkpoint', rstrt_file))
    print('{:14s}: {}'.format('LAT vector', pp_lat))

    # in all cases we use an initial state vector
    hd = 'Region-specific initialization state vectors to be used are:'
    print('\n{}'.format(hd))
    print('-' * len(hd) + '\n')

    for dkey, domain in cnf.items():

        # ignore null domains
        if domain is None:
            continue

        # get function defintion
        tags = domain.get('tags')
        fnc_name = domain.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)

        #
        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def, key))
        else:
            ep = fnc_def.get('EP')

            if ep is None:
                print('{:14s}: no EP defined in passive domain ({})'.format(dkey,tags))
                continue

            etags = None
            #if etags is not None:
             #   labels = expand_domain_labels(etags, labels)
            print('{:14s}: {} in ({})'.format(dkey, ep.get('init'), tags))

    print()


# ensure consistent prepacing checkpoint filenames for restarting
def chkpt_file_ID(prefix, ID, gen_mode, bcl):

    ID = '%s_%s_pp_%s_bcl_%.1f'%(prefix, ID, gen_mode, bcl)

    return ID


# ensure consistent lat filenames
def lat_file_ID(prefix, ID, gen_mode, ext):

    ID = '%s_%s_%s.%s'%(prefix, ID, gen_mode, ext)

    return ID


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_tissue_tuning'.format(today.isoformat())
    return out_DIR


@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|(imp_)|(tunecv_)')
def run(args, job):

    # read tissue/organ configuration
    with open(args.config) as cnf_fp:
        cnf = load(cnf_fp)

    # read experiment description
    with open(args.experiment) as fp:
        exps = load(fp)

    # retrieve definition of all functional domains
    fncs = exps.get('domains')

    # check configuration
    if not j2c.check_config(cnf, fncs):
        sys.exit()

    # use basename of mesh as ID
    meshname = exps.get('mesh')
    meshID = os.path.basename(meshname)

    # use consistent solver settings
    slv = exps.get('solver')

    # ---------- GEN-LAT --------------------------------------------
    if args.stage == 'gen-lat':
        # read in all electrode and relative timings used for prepacing
        elecs  = exps.get('electrodes')
        prtcls = exps.get('protocols')

        pp_prtcl    = prtcls.get('prepace')
        pp_lat_file = pp_prtcl.get('lat')
        propagation = pp_prtcl.get('propagation')
        mesh        = exps.get('mesh')

        # generate a new LAT vector if required
        lat_file = None
        lat_pfix = 'prepace_act_seq'
        lat_file = os.path.join('./init', lat_file_ID(lat_pfix, meshID, args.gen_lat, 'dat'))
        print('Generating {}-based lat vector file {}'.format(args.gen_lat, lat_file))

        cmd_lat, tmp_lat, simID = generate_lat(mesh, cnf, fncs, elecs, pp_prtcl, args.gen_lat, lat_file,
                                               args.visualize)
        
        # use rd run to generate activation sequence, additional options and checks required
        if args.gen_lat == 'rd':
            cmd_lat += j2c.add_slv_settings(slv)

            # before running, check all specified initial states are there
            if not j2c.check_initial_state_vectors(fncs, True):
                # some initial state vectors are missing
                print('\nMissing initial state vectors, exiting!\n\n')
                sys.exit()

        # generate lat vector
        job.carp(cmd_lat)

        # check whether lats file has been properly created
        print('\nCopying LAT vector')
        cp_cmd = ['cp', tmp_lat, lat_file]
        job.bash(cp_cmd)

        # update LAT vector in experiment description
        if not args.dry:
            pp_prtcl['lat'] = lat_file
            j2c.update_exp_desc(args.update, args.experiment, exps)

        if args.visualize:
            lat_mesh = os.path.join(simID, os.path.basename(mesh) + mesh_ext(args.gen_lat))
            job.meshalyzer(lat_mesh, lat_file)

    # ---------- PREPACE --------------------------------------------

    if args.stage == 'prepace':

        # read in all electrode and relative timings used for prepacing
        elecs  = exps.get('electrodes')
        prtcls = exps.get('protocols')

        if 'prepace' in prtcls:
            pp_prtcl    = prtcls.get('prepace')
            pp_lat_file = pp_prtcl.get('lat')
            propagation = pp_prtcl.get('propagation')
            mesh        = exps.get('mesh')

            # prepacing using lat, make sure we have a proper lat vector
            lat_file = None
            if args.prepace == 'lat' or args.prepace == 'lat1':
                # we need an activation lat vector
                if args.lat:
                    # command line provided gets precedence
                    lat_file = args.lat
                elif pp_lat_file is not None:
                    # experiment description provides lat
                    # check whether given lat file exists
                    if os.path.exists(pp_lat_file):
                        lat_file = pp_lat_file
                    else:
                        print('\nPrepacing lat file {} does not exist.'.format(pp_lat_file))

                if lat_file is None:
                    # we don't have a lat file, need to generate one
                    print('LAT-based prepacing requires LAT vector which is not provided.')
                    print(' 1) Provide lat file using --lat')
                    print(' 2) Or generate lat file first using --gen-lat ek or rd.\n')
                    sys.exit()

            # before running, check all specified initial states are there
            if not j2c.check_initial_state_vectors(fncs, True):
                # some initial state vectors are missing
                print('\nMissing initial state vectors, exiting!\n\n')
                sys.exit()

            # ready to run prepacing to create initial checkpoint for restarting
            print('Run prepacing in mode {}'.format(args.prepace))
            propagation = pp_prtcl.get('propagation')
            cmd_pp, statef, simID = prepace(mesh, cnf, fncs, elecs, pp_prtcl, args.prepace, lat_file, propagation,
                    args.visualize)

            cmd_pp += j2c.add_slv_settings(slv)

            # run
            job.carp(cmd_pp)

            if args.visualize:
                mesh = os.path.join(simID, os.path.basename(mesh) + mesh_ext(propagation))
                data = os.path.join(simID,'vm.igb')
                job.meshalyzer(mesh,data)


            # copy checkpoint to init directory for restarting
            print('\nCopying prepacing checkpoint file')
            chkpt_file = os.path.join(simID, statef + '.roe')
            #rstrt_file = os.path.join('./init', 'prepaced_' + args.prepace + '_bcl_%.1f'%(pp_prtcl.get('bcl')) + '.roe')
            rstrt_file = os.path.join('./init', chkpt_file_ID('prepaced', meshID, args.prepace, pp_prtcl.get('bcl')) + '.roe' )

            cp_cmd = ['cp', chkpt_file, rstrt_file]
            job.bash(cp_cmd)

            # summarize prepacing
            print_prepace_summary(cnf, exps, args.prepace, args.lat, rstrt_file)

        else:
            print('\nNo description of prepacing protocol found in {}.\n\n'.format(args.experiment))

    # ---------- VALIDATE --------------------------------------------

    if args.stage == 'validate':

        # read in all electrode and relative timings used for prepacing
        elecs  = exps['electrodes']
        prtcls = exps['protocols']
        pp_prtcl    = prtcls['prepace']
        pp_lat_file = pp_prtcl['lat']
        propagation = pp_prtcl.get('propagation')
        mesh        = exps['mesh']

        # pick prepacing checkpoint to restart from 
        chkpt_file = os.path.join('./init', chkpt_file_ID('prepaced', meshID, args.prepace, pp_prtcl.get('bcl')))

        cmd_chk, simID = restart(mesh, cnf, fncs, elecs, pp_prtcl, args.prepace, propagation, chkpt_file, args.visualize)
        cmd_chk += j2c.add_slv_settings(slv)

        # before running, check all specified initial states are there
        # as we are restarting, we should remove initial state vectors anyways as this is encoded in the checktpoint
        if not chk_initial_state_vectors(fncs, True):
            # some initial state vectors are missing
            print('\nMissing initial state vectors, exiting!\n\n')
            sys.exit()

        job.carp(cmd_chk)

        if args.visualize:
            rst_mesh = os.path.join(simID, os.path.basename(mesh) + mesh_ext(propagation))
            rst_data = os.path.join(simID, 'vm.igb')
            job.meshalyzer(rst_mesh, rst_data)


if __name__ == '__main__':
    run()
