#!/usr/bin/env python3

r"""

VARP experiment
===============

This experiments implements the Virtual Arrhythmia Risk Prediction experiment
as described by Arevalo et al in .

"""

EXAMPLE_DESCRIPTIVE_NAME = 'Virtual Arrhythmia Risk Prediction'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'


# --- import packages --------------------------------------------------------
import os
import sys
import json
import argparse
from shutil import copy
from datetime import date

from carputils import settings
from carputils import tools
from carputils import model
from carputils import ep
from carputils.resources import petsc_options
from carputils import testing


def parser():
    parser = tools.standard_parser()
    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--protocol',
            default='S1',
            choices=['S1', 'S2', 'MT', 'PR'],
            help='Pick protocol stage, possible choices are '
                +'S1 - pre-pacing with given number and cycle length of S1 stimuli '
                +'S2 - premature stimulus at short cycle length '
                +'MT - test VT induction and, if successfull, maintenance of VT'
                +'PR - proloing MT protocl to test longer term sustainance of VT' )

    group.add_argument('--stimuli', type=str, required=True,
                      help='Stimulus label as listed in the stims.json file in the chosen cohort directory.')

    group.add_argument('--stimulus', type=str, required=True,
                      help='Pick a specific stimulus location, stimulus label must be listed in stimuli json file.')

    group.add_argument('--pcl',
            default=370,
            type=float,
            help='Pacing cycle length of S1 stimulus train.'
                +'The default pacing cycle is 600 ms.')

    group.add_argument('--CI',
            default=330,
            type=float,
            help='Coupling interval for premature stimulus delivery (S2, S3 or S4).'
                +'The default is set to 330 ms.' )

    group.add_argument('--CI_min',
            default=280,
            type=float,
            help='Minimal coupling interval for premature stimulus delivery (S2, S3 or S4).'
                +'The default is set to 280 ms.'
                +'Using a pacing cycle length below this requires re-running the S1 stage' )

    group.add_argument('--cohort', type=str, required=True,
                      help='Directory storing cohort of all cases belonging to a given study')

    group.add_argument('--case', type=str, required=True,
                      help='Pick a specific case from the cohort directory.')

    group.add_argument('--domains', type=str, required=True,
                      help='JSON file holding the domain tags, must include tags BZ and SCAR.')

    group.add_argument('--sourceModel', default='monodomain',
                       choices=ep.MODEL_TYPES,
                       help='pick electrical model type (default: monodomain)')

    group.add_argument('--surf2vol',
            nargs=2,
            default=[0.14,0.14],
            help='Pick bidomain surface to volume ratio for normal and border zone tissue default: [ 0.14, 0.14 ]).'
                +'This scales velocity independently of direction. '
                +'Allows independent scaling of velocity in normal and border zone tisse. '
                +'This is approximated as 2/(cell radius) / fi '
                +'where fi is the intracellular volume fraction.')

    group.add_argument('--phie',
                       type=bool,
                       default=False,
                       help='Recover extracellular potentials in entire extracellular domain'
                           +'including heart and surrounding media (torso, blood pool, etc)' )

    group.add_argument('--prolong',
            default=1000, type=float,
            help='Prolong maintenance observation period (default: 1000).' )

    group.add_argument('--visual-mesh',
            default='N',
            choices=['N', 'X', 'XE'],
            help='Extract meshes for visualization, choices are '
                +'N  - do (N)ot extract meshes (default) '
                +'X  - e(X)tract meshes '
                +'XE - e(X)tract mesh and (E)xit, don not run simulation')

    group.add_argument('--pngs',
                       type=bool,
                       default=False,
                       help='Create png files from data using meshalyzer' )

#    group.add_argument('--propagation', default='R-E+',
#            choices=ep.PROP_TYPES,
#            help='pick propagation model (default: R-E-)')
#
#    group.add_argument('--EP',
#            default='TT2'm,
#            choices=['TT2','GPB_Land'],
#            help='pick human EP model (default is TT2)')
#
#    group.add_argument('--lats',
#            default = 'off',
#            choices = ['on','off'],
#            help = 'turn on/off computation of local activation times (default: off)')
#
#    group.add_argument('--ecg',
#            default = 'off',
#            choices = ['on','off'],
#            help = 'turn on/off computation of ECG (default: off)')
#
#    group.add_argument('--purkStim',
#            default='His',
#            choices= ['none','His'],
#            help='Purkinje system is stimulated via His trigger or not stimulated \
#                  (default: His)')
#
#    group.add_argument('--purkinje',
#            default='normal',
#            choices= ['none','normal','lbbb', 'rbbb', 'avps'],
#            help='Incorporate a Purkinje system (or not) and pick type of pathology \
#                  (default: normal)')
#    group.add_argument('--h4c-splitting',
#            default='on',
#            choices=['on','off'],
#            help='Whether to use node splitting to decouple the ventricles from the \
#                  atria (only for H4C geometry).(default: on)')
#
    return parser



def loadTagMap(args):
  # load tags from JSON file
  with open(args.domains) as fp:
    tag_dict = json.load(fp)

  # collect intra cellular regions
  intra = list()
  for key in tag_dict:
    # skip bath
    if key == 'BATH':
      continue

    value = tag_dict[key]
    if isinstance(value, list):
      intra += value
    elif isinstance(value, int):
      intra.append(value)
    else:
      print('Not supported type "{}" for tag "{}"!'.format(type(value), key))

  # add intra and extra regions

  tag_dict['INTRA'] = intra
  tag_dict['EXTRA'] = intra + list(tag_dict['BATH'].values())

  return argparse.Namespace(**tag_dict)


def loadStimMap(args):
  # load tags from JSON file
  with open(args.stimuli) as fp:
    stim_dict = json.load(fp)

  return stim_dict
  #argparse.Namespace(**stim_dict)


def Stage_IDs(args):
    
    # IDs of different stages
    # -----------------------
    S1_ID = simID('S1', args.stimulus, args.pcl, args.case)
    S2_ID = simID('S2', args.stimulus, args.CI, args.case)
    MT_ID = simID('MT', args.stimulus, args.CI, args.case)
    PR_ID = simID('PR', args.stimulus, args.CI, args.case)

    return [S1_ID, S2_ID, MT_ID, PR_ID]


def setup_protocol(args, stim_map, idx):
    """
    Set up protocol specific stimulus options.
    """

    # check whether stimulus is in list
    if args.stimulus not in stim_map:
        print('Stimulus %s not found in stimulus map file %s'%(args.stimulus,args.stimuli) )
        sys.exit()

    cmd = []

    # select electrode
    # ----------------
    electrode = [ '-stim[0].name', args.stimulus ]
    if 'VTXFILE' in stim_map[args.stimulus]:
        stm_vtx = os.path.join(args.cohort,'electrodes',stim_map[args.stimulus]['VTXFILE'])
        electrode += [ '-stim[{}].elec.vtx_file'.format(idx), stm_vtx]
    elif 'PARFILE' in stim_map[args.stimulus]:
        stm_par = os.path.join(args.cohort,'electrodes',stim_map[args.stimulus]['PARFILE'])
        electrode += [ '+F', stm_par]
    else:
        print('No valid stimulus electrode definition given for %s'%(stim_map[args.stimulus]) )

    cmd += electrode

    # IDs of different stages
    # -----------------------
    [S1_ID, S2_ID, MT_ID, PR_ID] = Stage_IDs(args)


    # Stage 1 - pre-pacing with S1 stimulus at PCL=600 ms
    # ---------------------------------------------------
    s1_start = 0         # start of protocol
    s1_pcl   = args.pcl  # pacing cycle length during S1
    s1_num   = 2         # number of S1 to deliver (typically 6, but we do less as we have pre-paced initial vectors
    s1_dur   = 2         # duration of a single pulse

    # we stop S1 at the earliest plausible S2 delivery
    # to checkpoint there for restarting without repeating anything
    s1_tend    = s1_pcl * (s1_num-1) + args.CI_min
    s1_stop    = s1_pcl * (s1_num-1)

    # Stage 2 - premature stimulus S2, S3 or S4 at prescribed PCL
    # -----------------------------------------------------------   
    s2_start = s1_stop + args.CI           # start of s2 stage
    s2_num   = 1                           # how many s2 to apply, typically we do 1 only
    s2_tend  = s2_start + args.CI          # end of s2 stage, observe for another S2 CI
    chk_intv = 10                          # checkpoint every 10 ms starting at chk0
    chk0     = s1_tend + chk_intv          # start checkpointing for restarting

    # Stage 3 - test maintenance of VT for at least 4 x PCL of until VT terminates
    # ----------------------------------------------------------------------------
    mt_tend = s2_tend + args.pcl * 4       # maintenance observation period after S2 delivery
    sent    = 10                           # sentinel, check for activity, stop otherwise

    # Stage 4 - test maintenance of VT for prolonged periods or until VT terminates
    # -----------------------------------------------------------------------------
    pr_tend = mt_tend + args.prolong       # maintenance observation period after end of MT

    # number of electrodes used is always 1
    nelecs = [ '-num_stim', 1 ]

    # pulse definition
    pulse = [ '-stim[0].pulse.name', "square-monophasic",
              '-stim[0].pulse.strength', 100 ]

    if args.protocol == 'S1':
        s1 = [ '-stim[0].ptcl.start',    s1_start,
               '-stim[0].ptcl.bcl',      s1_pcl,
               '-stim[0].ptcl.npls',     s1_num,
               '-stim[0].ptcl.duration', s1_dur ]
    
        s1_chk_pt = [ '-num_tsav', 1, 
                      '-tsav[0]',  s1_tend ]
       
        s1_prtcl = nelecs + s1 + pulse + [ '-tend', s1_tend ] + s1_chk_pt

        cmd += s1_prtcl

    if args.protocol == 'S2':
        restart = [ '-start_statef', './%s/state.%.1f'%(S1_ID,s1_tend) ]

        s2_chk_pt = [ '-chkpt_start', chk0,
                      '-chkpt_intv',  chk_intv,
                      '-chkpt_stop',  s2_start ] 

        s2 = [ '-stim[0].ptcl.start',    s2_start,
               '-stim[0].ptcl.bcl',      args.CI,
               '-stim[0].ptcl.npls',     s2_num,
               '-stim[0].ptcl.duration', s1_dur ]

        s2_prtcl = nelecs + s2 + pulse + [ '-tend', s2_tend ] + s2_chk_pt + restart 

        cmd += s2_prtcl

    # set up lat detection for sentinel, detect Vm upstrokes
    # by looking for positive slopes crossing the threshold
    lats = [ '-num_LATs',            1,
             '-lats[0].ID'       , 'Sentinel',
             '-lats[0].all'      ,   0,
             '-lats[0].measurand',   0,
             '-lats[0].threshold', -20,
             '-lats[0].mode'     ,   0 ]

    if args.protocol == 'MT':

        # we don't stimulate here anymore
        nelecs = [ '-num_stim', 0 ]

        # restart from end of S2 stage
        restart = [ '-start_statef', './%s/checkpoint.%.1f'%(S2_ID,s2_tend) ]

        # checkpoint for restarting prolongation period
        mt_chk_pt = [ '-num_tsav', 1, 
                      '-tsav[0]',  mt_tend ]

        mt_sentinel = [ '-sentinel_ID', 0,
                        '-t_sentinel_start', s2_tend, 
                        '-t_sentinel', sent ]
 
        mt_prtcl = nelecs + restart + mt_chk_pt + [ '-tend', mt_tend ] #+ lats + mt_sentinel

        cmd += mt_prtcl

    if args.protocol == 'PR':

        # we don't stimulate here anymore
        nelecs = [ '-num_stim', 0 ]
       
        # restart from end of S2 stage
        restart = [ '-start_statef', './%s/state.%.1f'%(MT_ID,mt_tend) ]

        # checkpoint for restarting prolongation period
        pr_chk_pt = [ '-num_tsav', 1, 
                      '-tsav[0]',  pr_tend ]

        pr_sentinel = [ '-sentinel_ID', 0,
                        '-t_sentinel_start', pr_tend, 
                        '-t_sentinel', sent ]

        pr_prtcl = nelecs + restart + pr_chk_pt + lats + pr_sentinel

        cmd += pr_prtcl

    return cmd    

def setup_imp_regions(args,tag_map):

    imp = [ '-num_imp_regions', 2]
    
    if not hasattr(tag_map,'NORMAL'):
        # go over normal regions, these are all INTRA regions except BZ
        tag_map.NORMAL = []
        for tag in tag_map.INTRA:
            if tag not in tag_map.BZ:
                tag_map.NORMAL += [tag]

    # assign regular non-infarcted EP
    nm_idx = 0
    imp += [ '-imp_region[{}].name'.format(nm_idx), 'NON_INFARCTED',
             '-imp_region[{}].im'.format(nm_idx), 'TT2',
             '-imp_region[{}].im_param'.format(nm_idx), '\'\'',
             '-imp_region[{}].im_sv_init'.format(nm_idx), './init/tt2_normal.sv']

    imp += [ '-imp_region[0].num_IDs', len(tag_map.NORMAL) ]
    for i,itag in enumerate(tag_map.NORMAL):
        imp += ['-imp_region[0].ID[%d]'%(i), itag ]
    # global scaling of conduction velocity in non-infarcted myocardium and BZ
    imp += [ '-imp_region[0].cellSurfVolRatio', args.surf2vol[0]]

    # assign regular non-infarcted EP
    bz_idx = 1
    imp += [ '-imp_region[{}].name'.format(bz_idx), 'BORDERZONE',
             '-imp_region[{}].name'.format(bz_idx), 'BZ',
             '-imp_region[{}].im'.format(bz_idx), 'TT2',
             '-imp_region[{}].im_param'.format(bz_idx), 'GNa-62%,GCaL-69%,Gkr-70%,Gks-80%',
             '-imp_region[{}].im_sv_init'.format(bz_idx), './init/tt2_bz.sv']

    imp += [ '-imp_region[1].num_IDs', len(tag_map.BZ)]
    for i,itag in enumerate(tag_map.BZ):
        imp += ['-imp_region[1].ID[%d]' % (i), itag]
    # global scaling of conduction velocity in non-infarcted myocardium and BZ
    imp += [ '-imp_region[1].cellSurfVolRatio', args.surf2vol[1]]

    return imp

def setup_conductive_regions(args,tag_map):

    # we define two gregions, NON_INFARCTED and BZ
    cmd = [ '-num_gregions', 2 ]

    nm_idx = 0
    cmd += [ '-gregion[{}].name'.format(nm_idx), 'NON_INFARCTED',
             # longitudinal 200um -> 0.6685 m/s, g_m = 0.1811
             '-gregion[{}].g_il'.format(nm_idx), 0.2550,
             '-gregion[{}].g_el'.format(nm_idx), 0.6250,
             # transverse 200um -> 0.343 m/s, g_hm = 0.0583
             '-gregion[{}].g_it'.format(nm_idx), 0.0775,
             '-gregion[{}].g_et'.format(nm_idx), 0.2360,
             '-gregion[{}].g_in'.format(nm_idx), 0.0775,
             '-gregion[{}].g_en'.format(nm_idx), 0.236
    ]

    cmd += [ '-gregion[{}].num_IDs'.format(nm_idx), len(tag_map.NORMAL)]
    for i,itag in enumerate(tag_map.NORMAL):
        cmd += ['-gregion[{}].ID[{}]'.format(nm_idx, i), itag ]

    bz_idx = 1
    cmd += [ '-gregion[{}].name'.format(bz_idx), 'BZ',
            # longitudinal 200um -> 0.6685 m/s, g_m = 0.1811
            '-gregion[{}].g_il'.format(bz_idx), 0.2550,
            '-gregion[{}].g_el'.format(bz_idx), 0.6250,
            # in bz, transverse is reduced by 90%
            # transverse 200um -> 0.343 m/s, g_hm = 0.0583
            '-gregion[{}].g_it'.format(bz_idx), 0.00775,
            '-gregion[{}].g_et'.format(bz_idx), 0.2360,
            '-gregion[{}].g_in'.format(bz_idx), 0.00775,
            '-gregion[{}].g_en'.format(bz_idx), 0.236
    ]
    cmd += [ '-gregion[{}].num_IDs'.format(bz_idx), len(tag_map.BZ)]
    for i,itag in enumerate(tag_map.BZ):
        cmd += ['-gregion[{}].ID[{}]'.format(bz_idx, i), itag ]
    return cmd



def jobID(args):
    """
    Generate name of top level output directory.
    """
    if args.protocol == 'S1':
        CI = args.pcl
    else:
        CI = args.CI

    return simID(args.protocol, args.stimulus, CI, args.case )


def simID(protocol, stimulus, CI, case):

     return '{}_{}_PCL_{}_ms_{}'.format(protocol, stimulus, CI, case )

def visual_mesh(job, args, tag_map):

    # assemble carp command line for mesh extraction
    cmd = tools.carp_cmd()
    
    # extract meshes if requrested
    cmd += tools.gen_physics_opts(tag_map.EXTRA, tag_map.INTRA)
    cmd += ['-gridout_i', 3,
            '-gridout_e', 3,
            '-experiment', 3 ]

    # prepare output of visualization meshes
    meshname = '{}/{}/{}'.format(args.cohort,args.case, args.case)
    simID    = '{}/{}/visual_mesh'.format(args.cohort,args.case)
    # check existence of directory
    if not os.path.isdir(simID):
        os.mkdir(simID)
    
    cmd  += [ '-simID', simID,
              '-meshname', meshname ]
    
    # Run mesh extraction command
    # change jobID
    job.ID = 'visual-mesh'
    job.carp(cmd)

    if args.visual_mesh == 'XE':
        sys.exit()


def make_pngs(job, args, meshname):
        
    # check if visualization meshes exist in case directory
    visual_meshes_dir = os.path.join(args.cohort,args.case,'visual_mesh')

    # Prepare file paths for transmembrane voltage
    geom = os.path.join(visual_meshes_dir, os.path.basename(meshname)+'_i')
    data = os.path.join(job.ID, 'vm.igb')
    view = os.path.join(args.cohort, 'vm.mshz')

    # create S1 stage
    IDs = Stage_IDs(args)

    for ID in IDs:
        data = os.path.join(ID, 'vm.igb')

        pngs = '--PNGfile=%s_pngs --numfr=10000 --frame=0'%(ID)

        job.meshalyzer(geom, data, pngs, view)

@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)')
def run(args, job):

    # load tag map
    tag_map = loadTagMap(args)

    # extract meshes if requrested
    if not args.visual_mesh == 'N':
        visual_mesh(job, args, tag_map)

    # load stimulus map
    stim_map = loadStimMap(args)

    # Get basic command line, including solver options
    cmd  = tools.carp_cmd('varp.par')

    cmd += ['-simID', job.ID ]

    # generate physics region INTRA and EXTRA
    cmd += tools.gen_physics_opts(tag_map.EXTRA, tag_map.INTRA)

    # select case from cohort
    meshname = '{}/{}/{}'.format(args.cohort,args.case, args.case)
    cmd += [ '-meshname', meshname ]

    # assign properties
    #cmd += [ 'imp_region[1].ID[0]', args.bz ]

    # pick protocol state at electrode 0
    idx = 0
    cmd += setup_protocol(args, stim_map, idx)

    # set up imp_regions NORMAL and BZ
    cmd += setup_imp_regions(args,tag_map)

    # set up conductive regions NORMAL and BZ
    cmd += setup_conductive_regions(args, tag_map)

    # recover extracellular potential field in a post-processing step
    if args.phie == True:
        ppID = 'pseudo_bidm'
        cmd += [ '-experiment', 4,
                 '-post_processing_opts', 128,
                 '-ppID', ppID, 
                 '-bidomain', 2,
                 '-ellip_use_pt', 1 ]

    if not args.pngs:
        # Run simulation
        job.carp(cmd)
    else:
        # png generation
        make_pngs(job, args, meshname)


    # Do visualization
    if args.visualize and not settings.platform.BATCH:

        
        # check if visualization meshes exist in case directory
        visual_meshes_dir = os.path.join(args.cohort,args.case,'visual_mesh')

        # Prepare file paths for transmembrane voltage
        geom = os.path.join(visual_meshes_dir, os.path.basename(meshname)+'_i')
        data = os.path.join(job.ID, 'vm.igb')
        view = os.path.join(args.cohort, 'vm.mshz')

        # Call meshalyzer Vm
        job.meshalyzer(geom, data, view)

        # Prepare file paths for extracellular potential at intracellular domain
        #geom = os.path.join(args.case, os.path.basename(meshname)+'_i')
        #data = os.path.join(job.ID, 'phie_i.igb')
        #view = os.path.join(visual_meshes_dir, 'phie.mshz')
        
        # Call meshalyzer phie_i
        #job.meshalyzer(geom, data, view)

        # Prepare file paths for extracellular potential over entire domain
        #geom = os.path.join(args.case, os.path.basename(meshname)+'_e')
        #data = os.path.join(job.ID, 'phie.igb')
        #view = os.path.join(visual_meshes_dir, 'phie_torso.mshz')
        
        # Call meshalyzer phie_i
        #job.meshalyzer(geom, data, view)


if __name__ == '__main__':
    run()
