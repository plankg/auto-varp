![auto-varp](./figs/auto-varp.png)

auto-varp - automated arrhythmia inducibility testing
=====================================================

The **auto-varp** repository contains a [**carputils**](https://opencarp.org) experiment
to support automated arrhythmia inducibility testing according to the 
**Virtual Arrhythmia Risk Prediction (VARP)** protocol.
Using auto-varp, VARP can be executed with arbitrary anatomical models and electrode configurations.

Technical requirements
----------------------

As all *in silico* experiments implemented in carputils, a working installation of 
both **opencarp** or **carpentry** and **carputils** are required.
Specifically, for **VARP** execution, only a **carp** simulator executable is required. 
For visualization, **meshalyzer** is used. 
For the purpose of testing [**varp**](https://www.nature.com/articles/ncomms11437) 
a simplified model of a tissue sheet with 
scar and isthmus region is generated 
which requires a working **mesher** executable.

Testing auto-varp
-----------------

To test auto-varp follow the following steps.

1) Create a cohort of simple slabs with infarct and isthmus test cases

```
cd cohorts/SLINF
make_SLINF_meshes.sh
cd ../..
```

2) Create the commands for executing the various stages of VARP

Use the `--dry``option first to see the commands

```
./varp.sh --dry
```

Execute these commands sequentially, or run `./varp.sh` without the `--dry` option.

After successfully completing the test, read below to create VARP experiments of your own.


Preparing a VARP experiment
---------------------------

VARP applies a stimulation protocol to induce a reentrant tachycardia in cardiac tissue
consisting of two types of tissue, **non-infarcted** tissue with normal electrophysiological (EP) properties,
and **border zone** tissue where EP properties are altered.

For executing VARP the following definitions must be provided:

**Cohort**
A cohort must be defined on which one would like to apply VARP. A cohort consists of a number of cases which are stored in a cohort directory.
The path to the cohort directory is provided through --cohort. All cases belonging to this cohort are stored in the cohort directory
in individual directories. Cases can be selected based on their directory name. As an example, a directory structure may look like

```
--COHORT XXX
  |
  -- CASE A
  |
  -- CASE B
  |
  -- CASE C
```

**Simulation domain**
Each case must be labelled according to the same standard. 
The simulation domain, by default, comprises the intracellular domain only.
The anatomical mesh must be labelled to discriminate the two types of tissues, 
that is, **non-infarcted** myocardium and **border zone**.
Scar is also an important region, but scars are not part of the intracellular domain.

As mesh labelling in anatomically realistic meshes may be laborious,
for the sake of flexibility the definition of the two tissues of interest can be prescribed
without mesh relabelling.
This is achieved  by providing a **domains** file where label sets can be defined and assigned to each of the two groups.
Group labels must be either **NORMAL** to store all labels considered to be healthy viable non-infarcted myocardium,
and **BZ** where all tissues considered to have altered border zone electrophysiological properties are gathered.
Any labels in the mesh that are not part of the intracellular domain are collectively gathered in an exatracellular **BATH** domain.
Labels are listed in a domains json file.

Labels must be used consistently across all cases in a given cohort!

An example of a **simple domain definition** for a mesh consisting of 5 labels comprising 
Blood (0), healthy myocardium (1), infarct border zone (11) and infarct isthmus region (12), and scar (15).

![auto-varp-slinf-domains](./figs/VARP_SLINF_labels.png)

These are regrouped in a domains.json file as follows

```
{
  "NORMAL": [1],  
  "BZ": [11, 12],
  "BATH": {
  "BLOOD" : 0,
  "SCAR": 15
 }
```

Here we decide between all lables forming the intracellular space, and the labels forming the bath domain.
Note that **SCAR** must be assigned to the bath domain. Any label of the intracellular space will be 
assigned non-infarcted EP properties, all labels listed under BZ will be assigned border zone EP properties.

In a more elaborate four chamber torso setup, where numerous labels can be defined, as shown below, an example json file 
could look as follows:

```
{
  "BZ": [6],
  "RV": [10],
  "RV_FAST": [11],
  "RV_SEPT_FAST": [12],
  "LV": [15],
  "LV_FAST": [16],
  "LV_SEPT_FAST": [17],
  "LA": [20],
  "RA": [30],
  "SINUS": [31],
  "SINUS_BLOCK": [34],
  "CT": [33],
  "PM": [34],
  "FO": [35],
  "FO_RIM": [36],
  "BB": [38],
  "RIPV": [40],
  "RSPV": [41],
  "LSPV": [42],
  "RSPV": [43],
  "BATH": {
  "TORSO": 0,
  "SCAR": 5,
  "VESSELS": 21,
  "LUNGS": 200,
  "RV_BP": 300,
  "LV_BP": 400,
  "VC_LIDS": 500,
  "PV_LIDS": 600
  }
}
```

![auto-varp-4ch-torso-domain](./figs/4CH_torso_domains_labels.png)

Again, the same rule is applied, all labels under BZ will be assigned border zone properties, 
the rest of the intracellular labels will be assigned healthy normal EP.

**Electrode positions**
Electrode positions must be defined for which VARP will be tested. 
All electrode definitions must be supplied using an electrode file in .json format. 
Definitions can be supplied in the form of a **carp** parameter file, specifying a volume
in Euclidian space or a tag_region, or, more directly, as **vertex** file that contains all the vertices 
forming the stimulation electrode. 

An example of a stimulus defintion is given in **stims.json** for the 2D testing setup.

```
{
  "S0600": {
     "PARFILE": "S0600.par"
  }, 
  "S0730": {     
     "PARFILE": "S0730.par"
  },
  "S0900":  {     
     "VTXFILE": "S0900"
  }
}
```

An electrode parameter file specifying the electrode at location 0600 must define a volume 
within which all vertices are considered forming the electrode:

```
{
stim[0].elec.p0[0] =  -1050 
stim[0].elec.p0[1] = -30050
stim[0].elec.p0[2] =   -500
stim[0].elec.p1[0] =   1050
stim[0].elec.p1[1] = -27950
stim[0].elec.p1[2] =    500
}
```

As VARP uses only one active electrode a time in each test, all electrodes used the same electrode index 0.

When using a vertex file, specified through the keyword VTXFILE, a mesh specific list of vertices must be provided. For example, defineingan electrode as the first four nodes in a mesh is done by

```
4
extra
0
1
2
3
```

where "extra" refers to the mesh the indices refer to. 

For simple geometries where all cases share the same geometry the PARFILE mechanisms is preferred. A volume-based definition of electrodes is geometry-based only and, as such, independent then of mesh discretization. For complex anatomical models this approach is not feasible as anatomies differ in size, shape, location and orientation. There, each electrode has to be explicitly described by a vertex list. To avoid manual laborious manual selection, or introducing an operator bias, definition should be based on universal ventricular coordinates (UVCs)  which facilitates the automated extraction of such vertex lists and the unique definition of locations that are at the  exact same positions relative to a given anatomy.

## Electrophysiological initialization

Initial state vectors for normal myocardium and borderzone must be created first. The TNNP model of human ventricular myocyte electrophysiology is used, configured for healthy and borderzone conditions. The initial state is generated using **bench** for the pacing cycle length, PCL, as used for VARP during the S1 stage of the protocol. By default, a PCL of 600 ms is used. Both types of cells are paced at the S1 PCL until a limit cycle is reached. The state vectors at the end of prepacing are stored then in the **init** directory, named as **`tt2_normal.sv`** and **`tt2_bz.sv`** for normal and border zone EP, respectively. These initial state vectors are available and do not need to be recreated, until one would like to change electrophysiology or the S1 PCL. 

### Initial state vector for non-infarcted myocardium (tt2_normal.sv)

```tt2_normal.sv_
-85.0273            # Vm
-                   # Lambda
-                   # delLambda
-                   # Tension
-                   # K_e
-                   # Na_e
-                   # Ca_e
0.00324532          # Iion
-                   # tension_component
-                   # illum
TT2
3.97991             # CaSR
0.00039304          # CaSS
0.138062            # Ca_i
3.4651e-05          # D
0.728822            # F
0.959179            # F2
0.99547             # FCaSS
3.98e-05            # GCaL
0.153               # Gkr
0.392               # Gks
0.294               # Gto
0.738495            # H
0.667078            # J
136.859             # K_i
0.00179074          # M
8.35924             # Na_i
2.50505e-08         # R
0.904421            # R_
0.999972            # S
0.0173906           # Xr1
0.469047            # Xr2
0.014472            # Xs
```

### Initial state vector for borzder zon (tt2_bz.sv)

```
-85.5942            # Vm
-                   # Lambda
-                   # delLambda
-                   # Tension
-                   # K_e
-                   # Na_e
-                   # Ca_e
0.000759707         # Iion
-                   # tension_component
-                   # illum
TT2
3.12007             # CaSR
0.000356464         # CaSS
0.10291             # Ca_i
3.21236e-05         # D
0.672672            # F
0.937291            # F2
0.999354            # FCaSS
1.2338e-05          # GCaL
0.0459              # Gkr
0.0784              # Gks
0.294               # Gto
0.754609            # H
0.6698              # J
138.52              # K_i
0.00158752          # M
6.85121             # Na_i
2.27531e-08         # R
0.972237            # R_
0.999973            # S
0.0236645           # Xr1
0.474954            # Xr2
0.0201831           # Xs
```

## VARP execution

After defining a cohort comprising a set of cases, the mesh labels and their membership for assigning EP and conductive properites, and all electrode positions to be probed for inducing, the VARP protocol can be executed. The protocol consists of a series of prepacing stimulations, S1, at a longer pacing cycle length, PCL, of 600 ms .

## Study definition

A VARP study is defined by specifying a cohort comprising a number of cases. 
All cases must use the same domain labels and the same names for electrodes.
This is specified by

* **`--cohort /path-to-cohort-directory`** to select cohort
* **`--case CASE_X`** to select case from cohort where `CASE_X` must be a directory name in the cohort directory 
* **`--domain DOMAINS.json`** to provide information on the labels and how these are combined to define the two key domains, **non-infarcted** and **border zone**.

If VARP is executed as published, no further parameters need to be provided. By default it is assumed that the PCL during S1 is 600ms. The default for shorter CIs in the S2-S4 stages is assumed to be in the range 280 to 330 ms, where S2 is delivered at 330 ms and S4 cannot be shorter than 280 ms. This can be overridden using

* **`--pcl PCL`** to specify another PCL than 600 ms
* **`--CI_min CI_MIN`** to set another minimally possible CI

If these parameters are chosen other than default they must be provided at all stages of the protocol. **`--CI_min`** is provided mainly for reasons of efficiency to facilitate a restart of the S2-4 protocol stage at. None of the S2-4 CIs can be chosen shorter than `CI_min`. If this is required the S1 stage must repeated with a smaller `--CI_min` specified.  


## S1 stage

At the initial S1 stage a train of stimuli is delivered from a given electrode location at a given PCL. 

![auto-varp-SLINF-S1-results](./figs/VARP_protocol_S1.png)

In contrast to the published protocol, only 2 instead of 6 stimuli are delivered as the tissue is initialized with limit cycle state vectors anyways. Beyond the selection of cohort and case and the domain labels which are constant throughout the entire protocol only two input parameters must be provided to select the protocol and the pacing location. These are selected by

* **`--protocol S1`** select S1 stage of the protocol
* **`--stimulus S0600`** select the pacing location

```
./varp.py --protocol S1 --pcl 600 --CI_min 280 --stimulus S0600 \
          --stimuli ./stims.json \ 
          --cohort ./cohorts/SLINF --case SLINF_0 \
          --domain ./slinf_domains.json
```
For each electrode simulation results are stored in the directories named **`S1_STIMULUS_PCL_XXX.Xms_CASE`**. For the settings above, the simulation output directory is named **`S1_S0600_PCL_600.0ms_SLINV_0`**.

Simulation results of the S1 stage for pacing from stimulus location `S0600` for the case `SLFIN_0` are shown below

[![Watch the video](./figs/PP_S0600_0007ms.png)](https://youtu.be/WHxX0QGpY0M)

## S2-S4 stage

Simulations are restarted from a checkpoint after the end of the S1 stage. The checkpoint refers to the shortest  possible CI, `CI_min`, chosen at the S1 stage. The CI to be probed first is S2 with the longest CI, typically 330 ms are used. Shorter CIs are used if the initial S2 fails to induce. In our implementation of the protocol the CI for S3 and S4 are progressively shortened, but in the output we always refer to S2, just with varying CIs.

![auto-varp-SLINF-S1-results](./figs/VARP_protocol_S2_4.png)


To start probing inducibility with a CI of 330 ms we run

```
./varp.py --protocol S2 --CI 330 --pcl 600 --CI_min 280 --stimulus S0600 \
          --stimuli ./stims.json \
          --cohort ./cohorts/SLINF --case SLINF_0 \
          --domain ./slinf_domains.json
```

**Note** `--pcl` and `--CI_min` must not be provided if the default settings are used, but must be provided otherwise as this information is required to determine the correct checkpoint from which to restart.

For each electrode simulation and CI results are stored in the directories named **`S2_STIMULUS_PCL_XXX.Xms_CASE`**. For the settings above, the simulation output directory is named **`S2_S0600_PCL_330.0ms_SLINV_0`**.

S2 induction mechanisms is illustrated in the video below. 
Shown is the state of the tissue at a CI of 325 ms relative to the last S1 stimulus delivered.
The video shows how S2 is blocked at the entrance to the isthmus 
and how this leads to VT. The circuit is characterized by one isthums 
and two outer loops.

[![Watch the video](./figs/S2_S0600_0325ms.png)](https://youtu.be/ia4DDreCbIs)


## Maintenance stage

To probe whether VT is maintained or not simulations are continued, restarting from the checkpoint ouput at the end of the S2 stage. The duration of the observation period to probe maintenance is set to 1000 ms. Parameters provided at the S2 stage must be given here as well, as this information is needed to select the correct checkpoint for restarting.

```
./varp.py --protocol MT --CI 330 --pcl 600 --CI_min 280 --stimulus S0600 \
          --stimuli ./stims.json \
          --cohort ./cohorts/SLINF --case SLINF_0
```     

## Prolongation stage

To probe whether VT is maintained for prolonged observation periodš simulations are continued, restarting from the checkpoint ouput at the end of the MT stage. The duration of the observation period to probe prolonged maintenance can be chosen by `--prolong PROLONG` where `PROLONG` is specified in milli seconds (default is 1000 ms). Parameters provided at the S2 stage must be given here as well, as this information is needed to select the correct checkpoint for restarting.

```
./varp.py --protocol PF --CI 330 --pcl 600 --CI_min 280 --stimulus S0600 \
          --stimuli ./stims.json \
          --cohort ./cohorts/SLINF --case SLINF_0
```

