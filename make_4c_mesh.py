#!/usr/bin/env python3

r"""
"""
import os
EXAMPLE_DESCRIPTIVE_NAME = 'Generate mesh with given number of tags for testing tissue initialization'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False

from datetime import date
from json import load,dump
import numpy as np


from carputils import settings
from carputils import tools
from carputils import model
from carputils import mesh



def parser():
    parser = tools.standard_parser()

    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--la', type=int, nargs='+', default = [20,21],
                      help='Add tags of left atrium.')
    group.add_argument('--ra', type=int, nargs='+', default = [22,23],
                      help='Add tags of right atrium.') 
    group.add_argument('--lv', type=int, nargs='+', default = [10,11],
                      help='Add tags of left ventricle.')
    group.add_argument('--rv', type=int, nargs='+', default = [15,16],
                      help='Add tags of right ventricle.')      
    return parser


def make_test_mesh(args, sz, res):

    # user input
    UM2MM      = 1./1000
    sz  = sz * UM2MM # in mm
    res = res* UM2MM # in mm
    #mshctr  = np.asarray([mshsize/2., mshsize/2., mshres/2.]) # in mm

    # generate mesh (mesh center will be the origin)
    geom = mesh.Block(size=(sz, sz, res), # in mm
                      #centre=mshctr,
                      resolution=res) # in mm
    geom.mesher_opts('test')

    # incorporate tag regions
    # Note: blocks need to be defined relative to the mesh centre

    # atrio-ventricular base - left heart
    ll_av_base  = np.asarray([-sz,-res,-res/2.]) # in mm
    ur_av_base  = np.asarray([-3*res, res, res/2.]) # in mm
    tag_av_base = 500
    av_base    = mesh.BoxRegion(ll_av_base, ur_av_base, tag_av_base) # in mm
    geom.add_region(av_base)

    # atrio-ventricular base - right heart
    ll_av_base  = np.asarray([res,-res,-res/2.]) # in mm
    ur_av_base  = np.asarray([ sz, res, res/2.]) # in mm
    tag_av_base = 500
    av_base    = mesh.BoxRegion(ll_av_base, ur_av_base, tag_av_base) # in mm
    geom.add_region(av_base)

    # left-right heart separation - ventricles
    ll_lr_heart = np.asarray([-res, -sz,-res/2.]) # in mm
    ur_lr_heart = np.asarray([ res,-3*res , res/2.]) # in mm
    tag_lr_heart=  600
    lr_heart    = mesh.BoxRegion(ll_lr_heart, ur_lr_heart, tag_lr_heart) # in mm
    geom.add_region(lr_heart)

    # left-right heart separation - atria 
    ll_lr_heart = np.asarray([-res, -res,-res/2.]) # in mm
    ur_lr_heart = np.asarray([ res, sz/2-3*res , res/2.]) # in mm
    tag_lr_heart=  600
    lr_heart    = mesh.BoxRegion(ll_lr_heart, ur_lr_heart, tag_lr_heart) # in mm
    geom.add_region(lr_heart)

    # region tags
    la_tags = np.asarray(args.la)
    ra_tags = np.asarray(args.ra)
    lv_tags = np.asarray(args.lv)
    rv_tags = np.asarray(args.rv)
    hx      = sz/2/np.asarray([len(la_tags),len(ra_tags), len(lv_tags), len(rv_tags)])

    # add LA
    h = hx[0]
    ll_la = np.asarray([-sz/2, 0, -res/2])
    of_la = np.asarray([ h, 0, 0])
    dg_la = np.asarray([ h, sz/2, res])

    for i,tag in enumerate(la_tags):
        ll = ll_la + i*of_la
        ur = ll + dg_la 
        rg = mesh.BoxRegion(ll, ur, tag) # in mm
        geom.add_region(rg)

    # add RA    
    h     = hx[1]
    ll_ra = np.asarray([ 0, 0, -res/2])
    of_ra = np.asarray([ h, 0, 0])
    dg_ra = np.asarray([ h, sz/2, res])

    for i,tag in enumerate(ra_tags):
        ll = ll_ra + i*of_ra
        ur = ll + dg_ra 
        rg = mesh.BoxRegion(ll, ur, tag) # in mm
        geom.add_region(rg)

    # add LV
    h = hx[2]
    ll_lv = np.asarray([-sz/2, -sz/2, -res/2])
    of_lv = np.asarray([ h, 0, 0])
    dg_lv = np.asarray([ h, sz/2, res])

    for i,tag in enumerate(lv_tags):
        ll = ll_lv + i*of_lv
        ur = ll + dg_lv 
        rg = mesh.BoxRegion(ll, ur, tag) # in mm
        geom.add_region(rg)

    # add RV
    h = hx[3]
    ll_rv = np.asarray([ 0, -sz/2, -res/2])
    of_rv = np.asarray([ h, 0, 0])
    dg_rv = np.asarray([ h, sz/2, res])

    for i,tag in enumerate(rv_tags):
        ll = ll_rv + i*of_rv
        ur = ll + dg_rv 
        rg = mesh.BoxRegion(ll, ur, tag) # in mm
        geom.add_region(rg)
    meshname = mesh.generate(geom)

    return meshname, geom


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_tissue_tuning'.format(today.isoformat())
    return out_DIR


@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|(imp_)')
def run(args, job):

    # read experiment description
    #with open(args.experiment) as fp:
    #    exps = load(fp)
    #    domains = exps['domains']

    meshname, geom = make_test_mesh(args, 10000, 250)

    
if __name__ == '__main__':
    run()
