#!/usr/bin/env python3

r"""
"""
import os, sys, argparse
from datetime import date
from json import load, dump
from shutil import copy
import numpy as np

from carputils import settings
from carputils import tools
from carputils import model
from carputils import mesh


EXAMPLE_DESCRIPTIVE_NAME = 'Tissue experiment velocity and conductivity tuning'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def parser():
    parser = tools.standard_parser()

    # experiment selection and generic overall options
    exp  = parser.add_argument_group('Experiment selection and general options')

    exp.add_argument('--experiment', type=str, required=True, default='exp.json',
                       help='Macro parameter experiment description in .json format.')
    
    exp.add_argument('--update', action='store_true',
                       help='Update conductivity and velocity settings in experiment description')

    # tuning stage arguments
    tune_args  = parser.add_argument_group('Tuning specific options')

    tune_args.add_argument('--tune-mode', 
                       choices=['gi', 'gm'], default='gm',
                       help='Select tuning strategy, change gi, gm or beta to obtain prescribed velocities.')

    tune_args.add_argument('--converge', action='store_true', default=False,
                       help='Enforce recomputation of conductivities to match given reference conduction velocities. ' + 
                            'This overrules settings in the experiment description to create a new set of conductivity values.' )

    tune_args.add_argument('--resolution',
                       type=float, default=argparse.SUPPRESS,
                       help='mesh resolution in [um], default is: 250. um' +
                            'if given, overrules any resolution settings  given in the --experiment file.' )

    tune_args.add_argument('--tag',
                       type=str, default='tune_cv',
                       help='Use tag for named output')


    return parser


def expand_domain_labels(etags, label_def):
    if isinstance(label_def, list):
        tags = list()
        for label in label_def:
            tags += expand_domain_labels(etags, label)
        return list(map(int, tags))
    elif isinstance(label_def, str):
        tags = etags[label_def]
        if isinstance(tags, list):
            return list(map(int, tags))
        elif isinstance(tags, int):
            return [tags]
        else:
            raise TypeError('Unknown tag type "{}" !'.format(type(label_def)))
    elif isinstance(label_def, int):
        return [label_def]
    else:
        raise TypeError('Unknown label type "{}" !'.format(type(label_def)))


def vel2List(CVs, G):

    v_list  = [CVs['vf'], CVs['vs']]
    gi_list = [G['gil'], G['git']]
    ge_list = [G['gel'], G['get']]
    axes    = ['f', 't']

    if CVs['vn'] != CVs['vs']:
        v_list.append(CVs['vn'])
        gi_list.append(G['gin'])
        ge_list.append(G['gen'])
        axes.append('n')

    return v_list, gi_list, ge_list, axes


def converge_CV(args, job, dkey, ep, cv, solver):

    G  = cv['G']

    length = 2      # length of strand for testing 
    tol    = 1e-4   # tolerance for velocity convergence
  
    cvsr = []
    cvs  = []
    gis  = []
    ges  = []
    gms  = []
    cvg  = False

    # reference velocities
    cvref = cv.get('CVref')
    if not cvref:
        return None

    cvsr, gis, ges, ax = vel2List(cv['CVref'], G)

    if args.converge:
        # converge to reference velocities
        cvg = True
        cvs = cvsr
    else:
        # use static conductivities to measure velocities, don't converge
        cvg = False
        cvs, gis, ges, ax = vel2List(cv['CVmeas'], G)

    # iterate over velocities
    tune_cv = {}
    for i, v in enumerate(cvs):

        # velocity must be > 0
        if v == 0: 
            print('Measured velocity CVmeas {} in {} must be > 0!'.format(ax[i], dkey))
            continue

        # create command line for each direction   
        cmd = [settings.execs.TUNECV, 
               '--np',         args.np,
               '--model',      ep['model'] ]

        # add optional parameters
        mpar   = ep.get('modelpar')
        svinit = ep.get('svinit')
        plugs  = ep.get('plugs')
        ppar   = ep.get('plugpar')
        if mpar:
            cmd += ['--modelpar', mpar ]
        if plugs:
            cmd += ['--plugs',  plugs ]
        if ppar:
            cmd += ['--plugpar', ppar ]
        if svinit:
            cmd += ['--svinit', svinit ]

        # spatial test resolution
        if 'resolution' in args:
            dx = args.resolution
        else:
            dx = cv.get('dx')

        cmd += ['--converge',   cvg,
                '--velocity',   v,
                '--resolution', dx,
                '--gi',         gis[i],
                '--ge',         ges[i],
                '--beta',       G.get('surf2vol'),
                '--dt',         solver.get('dt'),
                '--beqm',       solver.get('beqm'),
                '--ts',         solver.get('ts'),
                '--lumping',    solver.get('lumping'),
                '--opsplit',    solver.get('opsplit'),
                '--stol',       solver.get('stol'),
                '--tol',        str(tol),
                '--length',     str(length),
                '--stimS',      500,
                '--stimV',
                '--mode',       args.tune_mode,
                '--table',      '{}.dat'.format(args.tag),
                '--log',        'tunecv_{}_{}.log'.format(dkey, ax[i]),
                '--platform',   'desktop',
                '--silent']

        if args.visualize:
            cmd += ['--visualize']
    
        # run tuneCV 
        job.bash(cmd)

        with open('tune.json') as fp:
            tune = load(fp)
            tune_res = tune.get('results')
            if tune_res.get('vel') > 0.0:
                tune_cv[ax[i]] = tune
                # check for rotational isotropy
                if ax[i] == 't' and 'n' not in ax:
                    tune_cv['n'] = tune
            else:
                print()
                print('Measured velocity << than assumed %.1f'%(v))

    return tune_cv


def print_tune_cv_file(tune_file, exp_file):

    with open(tune_file) as fp:
        tune_cv = load(fp)

    with open(exp_file) as fp:
        exps = load(fp)

    domains = exps['domains']
    solver = exps['solver']
    print_tune_cv_results(tune_cv, domains, solver)


def print_tune_cv_results(tune_cv, domains, solver):

    print('\n\nTuning Summary')
    print('='*88)

    n_active = 0
    for dkey, res in tune_cv.items():
        
        # experimental settings in domain
        domain = domains.get(dkey)
        cvs    = domain.get('CV')
        on     = cvs.get('mutable')
        Gs     = cvs.get('G')
        dx     = cvs.get('dx')

        # skip empty results
        if res is None or not on:
            continue
        else:
            n_active = n_active + 1
        
        print('\nGlobal numerical settings')
        print('Grid resolution    = {:.1f} um'.format(dx))
        print('Surface-to-volume  = {:.4f} 1/cm'.format(Gs['surf2vol']))
        print('dt                 = {:.1f} um'.format(solver['dt']))
        print('time stepper       = {:d}'.format(solver['ts']))
        print('solver tolerance   = {:g}'.format(solver['stol']))
        print('mass lumping       = {}'.format(solver['lumping']))
        print('operator splitting = {}'.format(solver['opsplit']))
        # source model : monodomain
        # stimulus strength : 250.0

        print('\n\nVelocity and conductivity settings for domain {})'.format(dkey))
        print('='*88)

        # deal with inconsistent (historically) naming of axes
        cv_axs = ['vf', 'vs', 'vn']
        for i, ax in enumerate(['f', 't', 'n']):

            print('\nSettings along axis {}'.format(ax))
            print('-'*50)
            ax_data = res.get(ax)
            if ax_data is not None:
                r_ax = ax_data.get('results')
                print('    v{:s}   = {:.4f} m/s (v_ref = {:.3f} m/s)'.format(ax, r_ax['vel'], cvs['CVref'][cv_axs[i]]))
                print('    g_i{:s} = {:.4f} S/m'.format(ax, r_ax['gi']))
                print('    g_e{:s} = {:.4f} S/m'.format(ax, r_ax['ge']))
                print('    g_m{:s} = {:.4f} S/m'.format(ax, r_ax['gm']))
            else:
                print('Tuning of conduction velocity along axis {} failed'.format(ax))


        print('\n')    

    # inform in case there were no active domains
    if not n_active:
        print('Conduction velocity measurement/tuning not triggered.')
        print('No domains were active, set mutable true for domains of interest.\n')


def update_exp_CV(exps, tune_cv):

    for dkey in tune_cv:
        
        # tuning results
        res    = tune_cv.get(dkey)

        # experimental settings in domain
        domains = exps.get('domains')
        domain  = domains.get(dkey)
        cvs     = domain.get('CV')
        on      = cvs.get('mutable')
        Gs      = cvs.get('G')
        cvs_m   = cvs.get('CVmeas')

        if res is None:
            continue

        # deal with inconsistent (historically) naming of axes
        cv_axs  = ['vf',  'vs',  'vn']
        gi_axs  = ['gil', 'git', 'gin']
        ge_axs  = ['gel', 'get', 'gen']

        for i, ax in enumerate(['f', 't', 'n']):
            ax_data = res.get(ax)
            if ax_data is not None:
                # tuning along this axes worked, we do have data
                r_ax = ax_data.get('results')
                p_ax = ax_data.get('params')

                # update experiment with tuned values
                Gs[gi_axs[i]] = r_ax['gi']
                Gs[ge_axs[i]] = r_ax['ge']
                cvs_m[cv_axs[i]] = r_ax['vel']
            
                # update also surf2vol
                Gs['surf2vol'] = p_ax.get('beta')

    return exps


# update experimental description
def update_exp_desc(up_flag, exp_file, exp):

    if up_flag:
        # update experimental settings file
        fname = exp_file
    else:
        # dump to alternative file for review/comparison
        base, ext = os.path.splitext(exp_file)
        fname = '{}_tmp{}'.format(base, ext)

    print('Updating experimental settings to {}\n'.format(fname))

    with open(fname, 'w') as f:
        dump(exp, f, indent=4)


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_tissue_tuning'.format(today.isoformat())
    return out_DIR


@tools.carpexample(parser, jobID, clean_pattern='^(\d{4}-)|(imp_)|(tunecv_)')
def run(args, job):

    # read experiment description
    with open(args.experiment) as fp:
        exps = load(fp)

    domains = exps.get('domains', None)

    # get solver options
    solver = exps['solver']

    # compute limit cycle for each region, if mutable flag is set
    cv_tune = {}
    for dkey, domain in domains.items():
        ep = domain.get('EP', None)
        cv = domain.get('CV', None)
        if ep is None or cv is None:
            print('\nNo EP or CV defined for domain {} !\n'.format(dkey))
            continue
        if cv.get('mutable'):
            cv_tune[dkey] = converge_CV(args, job, dkey, ep, cv, solver)

            # save tuning results
            if not args.dry:
                with open('./{}/tune_cv.json'.format(job.ID), 'w') as f:
                    dump(cv_tune, f, indent=4)

    if not args.dry:

        # summarize tuning results for all domains
        print_tune_cv_results(cv_tune, domains, solver)

        # update tissue settings
        new_exp = update_exp_CV(exps, cv_tune)

        # update experiment description
        update_exp_desc(args.update, args.experiment, new_exp)


if __name__ == '__main__':
    run()
