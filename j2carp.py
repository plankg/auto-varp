import os
import json

def expand_domain_labels(etags, label_def):
    if isinstance(label_def, list):
        tags = list()
        for label in label_def:
            tags += expand_domain_labels(etags, label)
        return list(map(int, tags))
    elif isinstance(label_def, str):
        tags = etags[label_def]
        if isinstance(tags, list):
            return list(map(int, tags))
        elif isinstance(tags, int):
            return [tags]
        else:
            raise TypeError('Unknown tag type "{}" !'.format(type(label_def)))
    elif isinstance(label_def, int):
        return [label_def]
    else:
        raise TypeError('Unknown label type "{}" !'.format(type(label_def)))


# turn CVs and Gs of a CV section into list of values
# four lists are generated
# v_lst   [ v_f,  v_s,  (v_n)  ]
# gi_list [ gi_f, gi_s, (gi_n) ]
# gi_list [ gi_f, gi_s, (gi_n) ]
# axes    [ 'f',  't',  ('n')  ]
# values for normal axis are optional, in case of rotational isotropy n will be omitted
def vel2List(CVs, G):

    v_lst  = [CVs['vf' ], CVs['vs' ]]
    gi_lst = [G  ['gil'], G  ['git']]
    ge_lst = [G  ['gel'], G  ['get']]
    axes   = ['f', 't']

    #
    if CVs['vn'] != CVs['vs']:
        v_lst.append (CVs['vn' ])
        gi_lst.append(G  ['gin'])
        ge_lst.append(G  ['gen'])
        axes.append('n')

    return v_lst, gi_lst, ge_lst, axes


# translate EP dictionary sections to carp input
def ep_secs_to_carp(cnf,fncs):

    carp = []
    reg_cnt = 0
    for dkey, domain in cnf.items():

        if domain is None:
            continue

        # get function defintion
        tags     = domain.get('tags')
        fnc_name = domain.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)

        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def, dkey))

        else:
            ep = fnc_def.get('EP')
            if ep:
               carp += ep_sec_to_carp(tags, dkey, ep, reg_cnt)
            
            reg_cnt = reg_cnt + 1

    if reg_cnt > 0:
        carp = ['-num_imp_regions', reg_cnt ] + carp
        
    return carp


# translate single EP dictionary section to carp input
def ep_sec_to_carp(tags, key, ep, idx):

    reg = '-imp_region[{}]'.format(idx)

    #if etags is not None:
       # labels = expand_domain_labels(etags, labels)

    # name the imp_region
    carp = [reg+'.name', key ]

    # list IDs forming the G region
    carp += ['-imp_region[{}].num_IDs'.format(idx), len(tags)]
    for i, l in enumerate(tags):
        carp += ['-imp_region[{}].ID[{}]'.format(idx, i), l]

    # model is mandatory
    carp += [reg+'.im', ep['model']]

    modelpar = ep.get('modelpar', None)
    if modelpar and len(modelpar) > 0:
        carp += [reg+'.im_param', modelpar]

    init = ep.get('init', None)
    if init and len(init) > 0:
        carp += [reg+'.im_sv_init', init]
      
    return carp

# translate conductivity sections to carp input
def G_secs_to_carp(cnf, fncs):

    carp = []
    reg_cnt = 0
    for dkey, domain in cnf.items():

        if domain is None:
            continue

        # get function defintion
        tags     = domain.get('tags')
        fnc_name = domain.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)

        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def,dkey))

        else:

            cv = fnc_def.get('CV')

            # skip if CV not defined
            if cv is None:
                print('\nNo CV defined for function domain {} !\n'.format(fnc_def))
                continue

            carp += G_sec_to_carp(tags, dkey, cv['G'], reg_cnt)
            reg_cnt += 1

    carp = ['-num_gregions', reg_cnt] + carp

    return carp


# translate single conductivity section to carp input
def G_sec_to_carp(tags, key, G, idx):

    etags = None
    if etags is not None:
        labels = expand_domain_labels(etags, tags)

    # assign name
    carp = ['-gregion[{}].name'.format(idx), key]

    # list IDs forming the G region
    carp += ['-gregion[{}].num_IDs'.format(idx), len(tags)]
    for i, l in enumerate(tags):
        carp += ['-gregion[{}].ID[{}]'.format(idx, i), l]

    # assign conductivity values
    if G.get('gil') is not None:
        carp += ['-gregion[{}].g_il'.format(idx), G['gil'],
                 '-gregion[{}].g_it'.format(idx), G['git'],
                 '-gregion[{}].g_in'.format(idx), G['gin'],
                 '-gregion[{}].g_el'.format(idx), G['gel'],
                 '-gregion[{}].g_et'.format(idx), G['get'],
                 '-gregion[{}].g_en'.format(idx), G['gen']]
    else:
        # bath domain
        carp += ['-gregion[{}].g_bath'.format(idx), G['gbath'] ]

    return carp


# translate conduction velocity sections to carp input
def CV_secs_to_carp(cnf, fncs):

    # dictionary of defined functions
    fnc_lst = fncs.get('domains')

    carp = []
    ek_cnt = 0
    for dkey, domain in cnf.items():

        if domain is None:
            continue

        # get function defintion
        tags     = domain.get('tags')
        fnc_name = domain.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)
        
        #
        if not fnc_def:
            print('Function {} assigned in {} not defined.'.format(fnc_def,key))
        else:
            cv = fnc_def.get('CV')

            # skip if CV not defined
            if cv.get('CVref') is None:
                print('\nNo CV defined for function domain {} !\n'.format(dkey))
                continue

            carp += CV_sec_to_carp(cv, ek_cnt, dkey)
            ek_cnt = ek_cnt + 1

    carp = ['-num_ekregions', ek_cnt] + carp

    return carp


# translate single conduction velocity section to carp input
def CV_sec_to_carp(cv, idx, key):

    carp = ['-ekregion[{}].ID'.format(idx), idx,
            '-ekregion[{}].name'.format(idx), key,
            '-ekregion[{}].vel_f'.format(idx), cv['CVref']['vf'],
            '-ekregion[{}].vel_s'.format(idx), cv['CVref']['vs'],
            '-ekregion[{}].vel_n'.format(idx), cv['CVref']['vn']]
                
    return carp


# build grid intra
def domain_to_phys_region(ptype, name, domain, idx):
    
    preg = '-phys_region[{}]'.format(idx)
    phys_reg = [ preg+'.ptype',   ptype,
                 preg+'.name',    name,
                 preg+'.num_IDs', len(domain) ]

    for i, l in enumerate(domain):
        phys_reg += [ preg+'.ID[{}]'.format(i), l]

    return phys_reg


# define single lat detector for recording the activation sequence
def ek_lat_detect(idx, thr):

    lat = '-lats[{}]'.format(idx)

    lats  = [ lat + '.ID', 'vm_act_seq.dat',
              lat + '.measurand', 0,
              lat + '.all', 0,
              lat + '.mode', 0,
              lat + '.threshold', thr,
              lat + '.method', 1 ]

    return lats

def p_mode(mode):

    if mode in [ 'ek', 'EK', 'eikonal' ]:
        return 'ek'
    elif mode in [ 're', 'R-E+', 'R-E-'] :
        return 're'
    elif mode in [ 'rd', 'R-D' ]:
        return 'rd'
    else:
        print('\nUnknown propagation mode selected.\n')
        return None


# set up tissue properties
def setup_tissue(cnf, fncs, rmode):

    # define excitable regions based on the definition of EP
    i_domain = []
    e_domain = []
    for dkey in cnf:
        # any domain is part of extracellular
        tissue    = cnf.get(dkey)
        if tissue is None:
            continue

        n_domain = tissue.get('tags')
        fnc_name = tissue.get('func')

        # definition of function
        fnc_def = fncs.get(fnc_name)

        if not fnc_def:
            # function name is not defined
            print('Function {} not defined!'.format(fnc_name))
            continue

        # domains with define EP are part of intra
        e_domain += n_domain
        if fnc_def.get('EP') is not None:
            i_domain += n_domain

    etags = None
    if etags is not None:
        i_domain = expand_domain_labels(etags, i_domain)
        e_domain = expand_domain_labels(etags, e_domain)

    phys_regs = []
    if p_mode(rmode) == 'ek':
        # intra must be defined due to bug in carp
        phys_reg_in = domain_to_phys_region(0, 'intra',   i_domain, 0)
        phys_reg_ek = domain_to_phys_region(2, 'eikonal', i_domain, 1)
        phys_regs   = ['-num_phys_regions', 2] + phys_reg_in + phys_reg_ek

    elif p_mode(rmode) == 'rd':
        phys_reg_in = domain_to_phys_region(0, 'intra',   i_domain, 0)
        phys_reg_ex = domain_to_phys_region(1, 'extra',   e_domain, 1)
        phys_regs   = ['-num_phys_regions', 2] + phys_reg_in + phys_reg_ex

    elif p_mode(rmode) == 're':
        phys_reg_in = domain_to_phys_region(0, 'intra',   i_domain, 0)
        phys_reg_ex = domain_to_phys_region(1, 'extra',   e_domain, 1)
        phys_reg_ek = domain_to_phys_region(2, 'eikonal', i_domain, 2)
        phys_regs   = ['-num_phys_regions', 3] + phys_reg_in + phys_reg_ex + phys_reg_ek

    else:
        print('Unknown propagation mode {}.'.format(rmode))

    # set up gregion and ek_region
    g_regs  = G_secs_to_carp(cnf, fncs)

    ek_regs = []
    if p_mode(rmode) == 'ek' or p_mode(rmode) == 're':
        ek_regs += CV_secs_to_carp(cnf, fncs)

    # set up EP
    imp_regs = []
    if p_mode(rmode) != 'ek':
        imp_regs += ep_secs_to_carp(cnf, fncs)

    cmd  = imp_regs
    cmd += g_regs
    cmd += ek_regs
    cmd += phys_regs

    return cmd


# check configuration
def check_config(cnf, fncs):

    chk_msg = 'Checking configuration and function definitions'
    print('\n{}'.format(chk_msg))
    print('-'*len(chk_msg))

    max_len = 0 
    for dkey in cnf:
        if len(dkey) > max_len:
            max_len = len(dkey)

    ok = True
    for dkey, domain in cnf.items():
        # valid definition of a domain
        if domain:
            tags     = domain.get('tags')
            fnc_name = domain.get('func')

            # look up in function definition
            fnc_def = fncs.get(fnc_name)

            if fnc_def:
                print('Domain {} : tags {} use function {}'.format(dkey.ljust(max_len,' '), tags, fnc_name))
            else:
                print('Domain {} : tags {} use function {} -- not defined.'.format(dkey.ljust(max_len, ' '), tags, fnc_name))
                ok = False

    return ok


# add solver settings
def add_slv_settings(slv):

    cmd = []
    dt = slv.get('dt')
    if dt:
        cmd += [ '-dt', dt ]

    ts = slv.get('ts')
    if ts:
        cmd += [ '-parab_solve', ts ]

    mass_lump = slv.get('lumping')
    if mass_lump:
        cmd += [ '-mass_lumping', 1 ]
    else:
        cmd += [ '-mass_lumping', 0 ]

    opsplit = slv.get('opsplit')
    if opsplit:
        cmd += [ '-operator_splitting', 1 ]
    else:
        cmd += [ '-operator_splitting', 0 ]

    stol = slv.get('stol')
    if stol:
        cmd += [ '-cg_tol_parab', stol ]

    beqm = slv.get('beqm')
    if beqm:
        cmd += ['-bidm_eqv_mono', 1 ]
    else:
        cmd += ['-bidm_eqv_mono', 0 ]

    return cmd


# make sure all specified initial state vectors are found 
def check_initial_state_vectors(fncs, verbose):

    max_len = 0
    if verbose:
        print('\n')
        header = 'Checking specified initialization files:'
        print(header)
        print('-'*len(header))

        for key in fncs:
            if len(key) > max_len:
                max_len = len(key)

    missing = 0
    for key,fnc in fncs.items():
        if fnc:
            ep = fnc.get('EP')
            if ep:
                init_file = ep.get('init')
                if len(init_file):
                    if not os.path.exists(init_file):
                        if verbose:
                            print('{} : Initialization uses file {} -- missing.'.format(key.ljust(max_len,' '), init_file))
                        missing += 1
                    else:
                        if verbose:
                            print('{} : Initialization uses file {}.'.format(key.ljust(max_len,' '), init_file))

    if not missing:
        return True
    else:
        return False



# helper function to select stimulus mode
# voltage is more robust, but currently ignored in ek and re runs
# decide on stimulus type as function of propagation mode
def stim_mode(prp):

    # by defaul in rd runs, use voltage clamp to achieve robust capture
    s_type = 9
    s_strength = -30

    # eikonal mediated propagation picks up only transmembrane stimuli
    if p_mode(prp) == 'ek' or p_mode(prp) == 're':
        # eikonal-mediated triggers on transmembrane current stimuli
        s_strength = 250
        s_type     = 0

    return s_type, s_strength


# update experimental description
def update_exp_desc(up_flag, exp_file, exp):

    if up_flag:
        # update experimental settings file
        fname = exp_file
    else:
        # dump to alternative file for review/comparison
        base, ext = os.path.splitext(exp_file)
        fname = '{}_tmp{}'.format(base, ext)

    print('Updating experimental settings to {}'.format(fname))

    with open(fname, 'w') as f:
        json.dump(exp, f, indent=4)    
