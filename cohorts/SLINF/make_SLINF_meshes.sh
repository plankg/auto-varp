#!/bin/bash
mode=$1

mesher=mesher

# radius of scarred region in cm
scar_r=1.5
bz_r=1.6
d_isth=0.25

# length and width of tissue
sz=6

fiber_angles=( "0" "30" "45" "60" "90")

if [ "$mode" == "--clean" ]; then
    for fa in "${fiber_angles[@]}"
    do
        rm -rf SLINF_${fa}  
    done	
    exit
fi

for fa in "${fiber_angles[@]}"
do
  fiber_angle=${fa}
  case_name="SLINF_${fa}"
  msh_cmd="${mesher} -mesh ./${case_name}/${case_name} -size '{${sz} ${sz} 0.025}' -resolution '{250 250 250}' \
         -numRegions 3 \
         -regdef[0].type 0 -regdef[0].p0 '{-0.2 -1.5 -0.5}' -regdef[0].p1 '{0.25 1.5 0.5}' -regdef[0].tag 12 \
         -regdef[1].type 2 -regdef[1].p0 '{0. 0. -0.5}' -regdef[1].p1 '{0. 0. 0.5}' -regdef[1].rad ${scar_r} -regdef[1].tag 15 \
         -regdef[2].type 2 -regdef[2].p0 '{0. 0. -0.5}' -regdef[2].p1 '{0. 0. 0.5}' -regdef[2].rad ${bz_r}   -regdef[2].tag 11 \
         -fibers.rotEpi ${fiber_angle} -fibers.rotEndo ${fiber_angle}" 
  echo 
  echo ${msh_cmd}
  if [ "$mode" != "--dry" ]; then
       # check first whether directory exists
      if [ ! -d "./${case_name}" ]; then
          mkdir ./${case_name}
      fi   
      eval ${msh_cmd}
  fi  
done
