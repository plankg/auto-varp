#!/usr/bin/env python3

r"""
"""
import os

from datetime import date
from json import load, dump

from carputils import settings
from carputils import tools
from carputils import model

EXAMPLE_DESCRIPTIVE_NAME = 'Limit cycle experiment'
EXAMPLE_AUTHOR = 'Gernot Plank <gernot.plank@medunigraz.at>'
EXAMPLE_DIR = os.path.dirname(__file__)
GUIinclude = False


def parser():
    parser = tools.standard_parser()

    group  = parser.add_argument_group('experiment specific options')

    group.add_argument('--experiment', type=str, required=True, default='exp.json',
                       help='Macro parameter experiment description in .json format.')

    group.add_argument('--bcl',
                       type=float,
                       help='basic cycle length for limit cycle experiment in [ms].')

    group.add_argument('--cycles',
                       type=int, default=20,
                       help='Number of cycles to approximated stable limit cycle.')

    group.add_argument('--update', action='store_true',
                       help='Update ep initial state in experiment description')


    return parser


def get_sv_init_file_name(dkey, model, bcl, init_dir):
    f = init_dir+'/{}_{}_bcl_{:.1f}.sv'.format(dkey, model, bcl)
    return f


def print_init_summary(domains, cycles):
    
    print('Single cell limit cycle initialization completed.')
    print('='*88+'\n')

    print('{:10s} : {:8s} {:6s} {}'.format('cell', 'bcl [ms]', 'cycles', 'init vector'))
    print('-'*88)



    for dkey, domain in domains.items():

        ep = domain.get('EP')
    
        # skip domains without EP definitions
        if ep is None:
            print('\nNo EP defined for domain {} !\n'.format(dkey))
            continue

            ep['bcl'] = args.bcl
        
        mutable = ep.get('mutable')

        if mutable:
            print('{:10s} : {:5.1f} ms {:6d} {}'.format(dkey, ep.get('bcl'), cycles, ep.get('init')))


# initialize cell, launch limit cycle pacing experiment for a given cell
def init_cell(ep, cycs, job, lim_cyc_dir, dkey, visual):

    model = ep.get('model')
    bcl   = ep.get('bcl')

    # assemble name of file for storing initial state vector 
    init_file = get_sv_init_file_name(dkey, model, bcl, lim_cyc_dir)
    ep['init'] = init_file

    # configure cellular dynamics
    plg     = ep.get('plugs')
    plg_par = ep.get('plugspar')
    imp_par = ep.get('modelpar')

    cell_dyn = [ '--imp', ep.get('model') ]
    if imp_par and len(imp_par) > 0:
        cell_dyn += ['--imp-par', imp_par]

    if plg and len(plg) > 0:
        cell_dyn += ['--plug-in', plg_par]

    if plg_par and len(plg_par) > 0:
        cell_dyn += ['--plug-par', plg_par]
     

    # configure bench
    duration = cycs * bcl
    cmd = [ settings.execs.BENCH ] + cell_dyn
    cmd+= [ '--bcl',                bcl,
            '--stim-curr',          40,
            '--stim-dur',           1.,
            '--numstim',            cycs,
            '--duration',           duration,
            '--stim-start',         0,
            '--dt',                 20/1000,
            '--fout='               + job.ID + '/{}_limit_cycle'.format(dkey),
            '-S',                   duration,
            '-F',                   init_file,
            '--no-trace',           'on']

    if visual:
        cmd += ['--dt-out', 1.0,
                '--validate']
    else:
        cmd += ['--dt-out', duration]

    job.bash(cmd)

    return ep 


def jobID(args):
    """
    Generate name of top level output directory.
    """
    today = date.today()
    out_DIR = '{}_ep_init'.format(today.isoformat())
    return out_DIR


@tools.carpexample(parser, jobID)
def run(args, job):

    # read experiment description
    with open(args.experiment) as fp:
        exps = load(fp)

    # store all limit cycle initial state vectors in one place 
    lim_cyc_dir = './init'
    if not os.path.isdir(lim_cyc_dir):
        os.mkdir(lim_cyc_dir)

    # compute limit cycle for each region, if dynamic flag is set
    domains = exps['domains']
    n_act   = 0
    for dkey, domain in domains.items():

        ep = domain.get('EP')
        # skip domains without EP definitions
        if ep is None:
            print('\nNo EP defined for domain {} !\n'.format(dkey))
            continue

        # select bcl for limit cycle experiment
        if args.bcl is not None:
            print('Overruling bcl settings given at command line in {:s}.'.format(args.experiment))
            ep['bcl'] = args.bcl
        
        mutable = ep.get('mutable')

        print('\nDomain {:s}'.format(dkey))
        print('-'*(len(dkey)+7))

        if mutable:

            print('\nLimit cycle at bcl {:.1f} ms for {:d} cycles.\n'.format(ep.get('bcl'), args.cycles))

            n_act += 1
            ep = init_cell(ep, args.cycles, job, lim_cyc_dir, dkey, args.visualize)
          
            if args.update:
                with open(args.experiment, 'w') as fp:
                    dump(exps, fp, indent=4)

        else:

            # no limit cycle pacing
            print('\nNo limit cycle pacing, use pre-existing initial state vector {:s}.'.format(ep['init']))

            # provided init file not modified, but make sure that name is consistent
            sv_init = get_sv_init_file_name(dkey, ep.get('model'), ep.get('bcl'), lim_cyc_dir)

            # if file exist, rename to ensure consistent naming
            if ep['init'] != sv_init and os.path.isfile(ep['init']):
                print('Rename init file {:s} to {:s}.'.format(ep['init'], sv_init))
                os.rename(ep['init'], sv_init)
            print('\n\n')

    if n_act > 0:

        print_init_summary(domains, args.cycles)

    if args.visualize:

        print('Visualize limit cycle experiments')
        print('='*88+'\n\n')

        for dkey, domain in domains.items():

            ep = domain.get('EP', None)

            if ep is None:
                print('\nNo EP defined for domain {} !\n'.format(dkey))
                continue

            if ep.get('mutable'):
    
                print('\nVisualize {} limit cycle experiment.'.format(dkey))
                print('-'*88+'\n\n')
    
                # create binary state variable trace file
                b2h5 = [settings.execs.SV2H5B, 
                        '{}/{}_limit_cycle_header.txt'.format(job.ID, dkey),
                        '{}/{}.h5'.format(job.ID, dkey)]
                job.bash(b2h5)
    
                # create binary state variable trace file
                vh5 = [settings.execs.LIMPETGUI, 
                       '{}/{}.h5'.format(job.ID, dkey)]
                    
                job.bash(vh5)

            else:
                print('\nLimit cycle experiment not executed for {}.'.format(dkey))
                print('Set mutable flag in {} EP section of {}.'.format(dkey,args.experiment))
                print('-'*88+'\n\n')

if __name__ == '__main__':
    run()
