#!/bin/bash
mode=$1

# input parameters
cohort="./cohorts/SLINF"
case="SLINF_0"
stimuli="./stims.json"
stim="S0600"
domain="./slinf_domains.json"
S1_pcl=600
S2_CI_min=280

# pick suitable number of cores 
np=4

# initialization, create cohort first
if [ "$mode" == "--init" ]; then
  cd ${cohort}
  cmd="./make_SLINF_meshes.sh"
  echo 
  echo "creating cohort meshes ..."
  echo ${cmd}
  eval ${cmd}
  exit
fi

# S1 stage of protocol
# For now, the following aspects are hardcoded
# CL is fixed here to 600 ms
# The number of S1 deliveries is limited to 2 (instead of 6), for the sake of speeding things up

S1="./varp.py --protocol S1 --stimuli ${stimuli} --stimulus ${stim} --pcl ${S1_pcl} --CI_min ${S2_CI_min} \
              --cohort ./cohorts/SLINF --case ${case} --domain ${domain} --np ${np}"

echo
echo "Prepacing S1 protocol"
echo "---------------------"
echo
echo ${S1}
echo

if [ "$mode" != "--dry" ]; then 
  eval ${S1}
fi

# S2 stage of protocol, we shorten the CI now
S2_CI=325
S2="./varp.py --protocol S2 --CI ${S2_CI} --stimuli ${stimuli} --stimulus ${stim} --pcl ${S1_pcl} --CI_min ${S2_CI_min} \
              --cohort ./cohorts/SLINF --case ${case} --domain ${domain} --np ${np}"

echo
echo "Prepacing S2 protocol"
echo "---------------------"
echo
echo ${S2}
echo

if [ "$mode" != "--dry" ]; then 
  eval ${S2}
fi

# MT stage of protocol, we restart from the S2 stage
MT="./varp.py --protocol MT --CI ${S2_CI} --stimuli ${stimuli} --stimulus ${stim} --pcl ${S1_pcl} --CI_min ${S2_CI_min} \
              --cohort ./cohorts/SLINF --case ${case} --domain ${domain} --np ${np}"

echo
echo "Maintenance testing MT protocol"
echo "-------------------------------"
echo
echo ${MT}
echo

if [ "$mode" != "--dry" ]; then 
  eval ${MT}
fi


# PR stage of protocol, we restart from the MT stage
PRL_DUR=2000
PR="./varp.py --protocol PR --prolong ${PRL_DUR} --CI ${S2_CI} --stimuli ${stimuli} --stimulus ${stim} --pcl ${S1_pcl} --CI_min ${S2_CI_min} \
              --cohort ./cohorts/SLINF --case ${case} --domain ${domain} --np ${np}"

echo
echo "Maintenance testing PR protocol over ${PRL_DUR} ms"
echo "--------------------------------------------------"
echo
echo ${PR}
echo

if [ "$mode" != "--dry" ]; then 
  eval ${PR}
fi
