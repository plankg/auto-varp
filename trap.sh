#/bin/bash

np=4

# make sure that converged conduction velocities are correct in 3D for planar wavefronts
# measured varp conduction velocities are:
#
#  healthy myocardium: vf = xx    vs,n = yy
#  borderzone        : vf = 0.592 vs,n = 0.107

# tissue dimension chosen such that planar wavefront arrive at opposite face after 40 ms
# lf = 23.7 mm
# ls =  4.3 mm

# tissue dimension borderzon settings
l_f=23.7
l_s=4.3


# converged dx is taken as 50 um
dx=50

# test planar wavefront along f
cmd="./tuneCV3.py --experiment varp.json --duration 50 --stim-geometry f --resolution $dx --visualize --np ${np} --lf ${l_f} --ls 0.0 --ln ${l_s} --d 0.5 --ID trap_planar_f"
echo $cmd

# test planar wavefront along s
cmd="./tuneCV3.py --experiment varp.json --duration 50 --stim-geometry s --resolution $dx --visualize --np ${np} --lf  0.0 --ls ${l_s} --ln ${l_s} --d 0.1 --ID trap_planar_s"
echo $cmd

# test elliptic wavefront velocities, use spherical stimulus to initiate as closely as possible an elliptic wavefront
cmd="./tuneCV3.py --experiment varp.json --duration 50 --stim-geometry sphere --resolution $dx --visualize --np ${np} --lf ${l_f} --ls ${l_s} --ln ${l_s} --d 0.1 --ID trap_elliptic_conv"
echo $cmd

# run spatial discretization variations
dxs=("300" "400" "500" "550" "600")

for dx in ${dxs[@]}; do
    cmd="./tuneCV3.py --experiment varp.json --duration 120 --stim-geometry sphere --resolution $dx --visualize --np ${np} --lf ${l_f} --ls ${l_s} --ln ${l_s} --d 0.5
--ID trap_elliptic_dx_${dx}"
    echo $cmd

done
